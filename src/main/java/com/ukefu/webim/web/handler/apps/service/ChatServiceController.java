package com.ukefu.webim.web.handler.apps.service;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.domain.Sort.Order;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.ukefu.core.UKDataContext;
import com.ukefu.util.IP;
import com.ukefu.util.Menu;
import com.ukefu.util.UKTools;
import com.ukefu.util.client.NettyClients;
import com.ukefu.util.extra.DataExchangeInterface;
import com.ukefu.util.extra.EkmDataInterface;
import com.ukefu.util.task.export.ExcelExporterProcess;
import com.ukefu.webim.service.acd.ServiceQuene;
import com.ukefu.webim.service.cache.CacheHelper;
import com.ukefu.webim.service.es.OnlineUserESRepository;
import com.ukefu.webim.service.es.QuickReplyRepository;
import com.ukefu.webim.service.repository.AgentServiceRepository;
import com.ukefu.webim.service.repository.AgentStatusRepository;
import com.ukefu.webim.service.repository.AgentUserContactsRepository;
import com.ukefu.webim.service.repository.AgentUserRepository;
import com.ukefu.webim.service.repository.ChatMessageRepository;
import com.ukefu.webim.service.repository.ConsultInviteRepository;
import com.ukefu.webim.service.repository.LeaveMsgRepository;
import com.ukefu.webim.service.repository.MetadataRepository;
import com.ukefu.webim.service.repository.OrganRepository;
import com.ukefu.webim.service.repository.OrgiSkillRelRepository;
import com.ukefu.webim.service.repository.PbxHostRepository;
import com.ukefu.webim.service.repository.QuickTypeRepository;
import com.ukefu.webim.service.repository.SNSAccountRepository;
import com.ukefu.webim.service.repository.ServiceAiRepository;
import com.ukefu.webim.service.repository.ServiceSummaryRepository;
import com.ukefu.webim.service.repository.SessionTypeRepository;
import com.ukefu.webim.service.repository.StatusEventRepository;
import com.ukefu.webim.service.repository.SysDicRepository;
import com.ukefu.webim.service.repository.TagRelationRepository;
import com.ukefu.webim.service.repository.TagRepository;
import com.ukefu.webim.service.repository.UserRepository;
import com.ukefu.webim.service.repository.WeiXinUserRepository;
import com.ukefu.webim.service.sns.SnsAccountUtil;
import com.ukefu.webim.util.OnlineUserUtils;
import com.ukefu.webim.util.server.message.ChatMessage;
import com.ukefu.webim.web.handler.Handler;
import com.ukefu.webim.web.model.AgentService;
import com.ukefu.webim.web.model.AgentServiceSummary;
import com.ukefu.webim.web.model.AgentStatus;
import com.ukefu.webim.web.model.AgentUser;
import com.ukefu.webim.web.model.AgentUserContacts;
import com.ukefu.webim.web.model.AiUser;
import com.ukefu.webim.web.model.EkmKnowledgeMaster;
import com.ukefu.webim.web.model.LeaveMsg;
import com.ukefu.webim.web.model.MetadataTable;
import com.ukefu.webim.web.model.OnlineUser;
import com.ukefu.webim.web.model.Organ;
import com.ukefu.webim.web.model.OrgiSkillRel;
import com.ukefu.webim.web.model.PbxHost;
import com.ukefu.webim.web.model.QuickType;
import com.ukefu.webim.web.model.SNSAccount;
import com.ukefu.webim.web.model.ServiceAi;
import com.ukefu.webim.web.model.SessionConfig;
import com.ukefu.webim.web.model.SessionType;
import com.ukefu.webim.web.model.StatusEvent;
import com.ukefu.webim.web.model.SysDic;
import com.ukefu.webim.web.model.User;
import com.ukefu.webim.web.model.WeiXinUser;

import freemarker.template.TemplateException;

@Controller
@RequestMapping("/service")
public class ChatServiceController extends Handler{
	
	@Autowired
	private AgentServiceRepository agentServiceRes ;
	
	@Autowired
	private AgentUserRepository agentUserRes ;
	
	@Autowired
	private AgentStatusRepository agentStatusRepository ;
	
	@Autowired
	private AgentUserRepository agentUserRepository ;
	
	@Autowired
	private LeaveMsgRepository leaveMsgRes ;
	
	@Autowired
	private OrganRepository organRes ;
	
	@Autowired
	private OrganRepository organ ;
	
	@Autowired
	private MetadataRepository metadataRes ;
	
	@Autowired
	private UserRepository user ;
	
	@Autowired
	private UserRepository userRes ;
	@Autowired
	private OrgiSkillRelRepository orgiSkillRelService;
	@Autowired
	private ChatMessageRepository chatMessageRes;
	
	@Autowired
	private ServiceAiRepository serviceAiRes ;
	
	@Autowired
	private ConsultInviteRepository inviteRepository;
	
	@Autowired
	private ServiceSummaryRepository serviceSummaryRes ;
	
	@Autowired
	private ChatMessageRepository chatMessageRepository ;
	
	@Autowired
	private OnlineUserESRepository onlineUserESRes;
	
	@Autowired
	private WeiXinUserRepository weiXinUserRes;
	
	@Autowired
	private AgentServiceRepository agentServiceRepository;
	
	@Autowired
	private StatusEventRepository statusEventRes ;
	
	@Autowired
	private PbxHostRepository pbxHostRes ;
	
	@Autowired
	private TagRepository tagRes ;
	
	@Autowired
	private TagRelationRepository tagRelationRes ;
	
	@Autowired
	private QuickReplyRepository quickReplyRes ;
	
	@Autowired
	private QuickTypeRepository quickTypeRes ;
	
	@Autowired
	private SysDicRepository sysDicRes ;
	
	@Autowired
	private SessionTypeRepository sessionTypeRes ;
	
	@Autowired
	private SNSAccountRepository snsAccountRes ;
	
	@Autowired
	private AgentUserContactsRepository agentUserContactsRes; 
	
	@RequestMapping("/history/index")
    @Menu(type = "service" , subtype = "history")
    public ModelAndView index(ModelMap map , HttpServletRequest request ,final String username,final String channel ,final String servicetype,final String skill,final String agent,final String servicetimetype,final String begin,final String end , final String sbegin,final String send) {
		final String orgi = super.getOrgi(request);
		Page<AgentService> page = agentServiceRes.findAll(new Specification<AgentService>(){
			@Override
			public Predicate toPredicate(Root<AgentService> root, CriteriaQuery<?> query,CriteriaBuilder cb) {
				List<Predicate> list = new ArrayList<Predicate>();  
				if(!StringUtils.isBlank(username)) {
					list.add(cb.equal(root.get("username").as(String.class), username)) ;
				}
				
				list.add(cb.equal(root.get("orgi").as(String.class), orgi)) ;
				
				if(!StringUtils.isBlank(channel)) {
					list.add(cb.equal(root.get("channel").as(String.class), channel)) ;
				}
				if(!StringUtils.isBlank(agent)) {
					list.add(cb.equal(root.get("agentno").as(String.class), agent)) ;
				}
				if(!StringUtils.isBlank(skill)) {
					list.add(cb.equal(root.get("agentskill").as(String.class), skill)) ;
				}
				try {
					if(!StringUtils.isBlank(begin) && begin.matches("[\\d]{4}-[\\d]{2}-[\\d]{2} [\\d]{2}:[\\d]{2}:[\\d]{2}")){
						list.add(cb.greaterThanOrEqualTo(root.get("logindate").as(Date.class), UKTools.dateFormate.parse(begin))) ;
					}
					if(!StringUtils.isBlank(end) && end.matches("[\\d]{4}-[\\d]{2}-[\\d]{2} [\\d]{2}:[\\d]{2}:[\\d]{2}")){
						list.add(cb.lessThanOrEqualTo(root.get("logindate").as(Date.class), UKTools.dateFormate.parse(end))) ;
					}
					
					if(!StringUtils.isBlank(sbegin) && sbegin.matches("[\\d]{4}-[\\d]{2}-[\\d]{2} [\\d]{2}:[\\d]{2}:[\\d]{2}")){
						list.add(cb.greaterThanOrEqualTo(root.get("servicetime").as(Date.class), UKTools.dateFormate.parse(sbegin))) ;
					}
					if(!StringUtils.isBlank(send) && send.matches("[\\d]{4}-[\\d]{2}-[\\d]{2} [\\d]{2}:[\\d]{2}:[\\d]{2}")){
						list.add(cb.lessThanOrEqualTo(root.get("servicetime").as(Date.class), UKTools.dateFormate.parse(send))) ;
					}
				} catch (ParseException e) {
					e.printStackTrace();
				}
				
				Predicate[] p = new Predicate[list.size()];  
			    return cb.and(list.toArray(p));   
			}
		},new PageRequest(super.getP(request), super.getPs(request), Direction.DESC , "createtime")) ;
		for(AgentService service : page.getContent()) {
			if(!StringUtils.isBlank(service.getAppid())) {
				SNSAccount snsAccount = SnsAccountUtil.process(service.getAppid() ,service.getOrgi()) ;
				if(snsAccount!=null) {
					service.setSnsname(snsAccount.getName());
				}
			}
		}
		map.put("agentServiceList", page) ;
		map.put("username", username) ;
		map.put("channel", channel) ;
		map.put("servicetype", servicetype) ;
		map.put("servicetimetype", servicetimetype) ;
		map.put("agent", agent);
		map.put("skill", skill);
		map.put("begin", begin) ;
		map.put("end", end) ;
		map.put("sbegin", sbegin) ;
		map.put("send", send) ;
		map.put("organlist",organ.findByOrgi(super.getOrgi(request)));
		map.put("userlist",user.findByOrgiAndDatastatus(super.getOrgi(request), false));
		List<ServiceAi> aiList = this.serviceAiRes.findByOrgi(super.getOrgi(request));
		map.addAttribute("aiList", aiList);
        return request(super.createAppsTempletResponse("/apps/service/history/index"));
    }
	
	@RequestMapping("/current/index")
    @Menu(type = "service" , subtype = "current" )
    public ModelAndView current(ModelMap map , HttpServletRequest request) {
		Page<AgentService> page = agentServiceRes.findByOrgiAndStatus(super.getOrgi(request), UKDataContext.AgentUserStatusEnum.INSERVICE.toString() ,new PageRequest(super.getP(request), super.getPs(request), Direction.DESC , "createtime")) ;
		map.put("agentServiceList", page) ;
		
		for(AgentService service : page.getContent()) {
			if(!StringUtils.isBlank(service.getAppid())) {
				SNSAccount snsAccount = SnsAccountUtil.process(service.getAppid() ,service.getOrgi()) ;
				if(snsAccount!=null) {
					service.setSnsname(snsAccount.getName());
				}
			}
		}
		
		List<ServiceAi> aiList = this.serviceAiRes.findByOrgi(super.getOrgi(request));
		map.addAttribute("aiList", aiList);
		return request(super.createAppsTempletResponse("/apps/service/current/index"));
    }
	
	@RequestMapping("/current/trans")
    @Menu(type = "service" , subtype = "current")
    public ModelAndView trans(ModelMap map , HttpServletRequest request , @Valid String id) {
		if(!StringUtils.isBlank(id)){
			AgentService agentService = agentServiceRes.findByIdAndOrgi(id, super.getOrgi(request)) ;
			List<Organ> skillList = OnlineUserUtils.organ(super.getOrgi(request),true) ;
			String currentOrgan = super.getUser(request).getOrgan();
			if(StringUtils.isBlank(currentOrgan)) {
				if(!skillList.isEmpty()) {
					currentOrgan = skillList.get(0).getId();
				}
			}
			List<AgentStatus> agentStatusList = ServiceQuene.getAgentStatus(null , super.getOrgi(request));
			List<String> usersids = new ArrayList<String>();
			if(!agentStatusList.isEmpty()) {
				for(AgentStatus agentStatus:agentStatusList) {
					if(agentStatus!=null){
						usersids.add(agentStatus.getAgentno()) ;
					}
				}
				
			}
			List<User> userList = userRes.findAll(usersids);
			for(User user : userList){
				user.setAgentStatus((AgentStatus) CacheHelper.getAgentStatusCacheBean().getCacheObject(user.getId(), super.getOrgi(request)));
			}
			map.addAttribute("userList", userList) ;
			map.addAttribute("userid", agentService.getUserid()) ;
			map.addAttribute("agentserviceid", agentService.getId()) ;
			map.addAttribute("agentuserid", agentService.getAgentuserid()) ;
			map.addAttribute("agentservice", agentService) ;
			map.addAttribute("skillList", skillList) ;
			map.addAttribute("currentorgan", currentOrgan) ;
		}
		
		return request(super.createRequestPageTempletResponse("/apps/service/current/transfer"));
    }
	
	@RequestMapping(value="/transfer/save")  
	@Menu(type = "apps", subtype = "transfersave")
    public ModelAndView transfersave(ModelMap map , HttpServletRequest request , @Valid String id , @Valid String agentno , @Valid String memo){ 
		if(!StringUtils.isBlank(id)){
			AgentService agentService = agentServiceRes.findByIdAndOrgi(id, super.getOrgi(request)) ;
			AgentUser agentUser = (AgentUser) CacheHelper.getAgentUserCacheBean().getCacheObject(agentService.getUserid(), super.getOrgi(request)) ;
			if(agentUser != null){
				agentUser.setAgentno(agentno);
				CacheHelper.getAgentUserCacheBean().put(agentService.getUserid() , agentUser , super.getOrgi(request)) ;
				agentUserRepository.save(agentUser) ;
				{		//转接 ， 发送消息给 目标坐席
					AgentStatus agentStatus = (AgentStatus) CacheHelper.getAgentStatusCacheBean().getCacheObject(super.getUser(request).getId(), super.getOrgi(request)) ;
					
					if(agentStatus!=null){
						ServiceQuene.updateAgentStatus(agentStatus, agentUser, super.getOrgi(request), false);
					}
					
					AgentStatus transAgentStatus = (AgentStatus) CacheHelper.getAgentStatusCacheBean().getCacheObject(agentno, super.getOrgi(request)) ;
					if(transAgentStatus!=null){
						ServiceQuene.updateAgentStatus(transAgentStatus, agentUser, super.getOrgi(request), true);
						agentService.setAgentno(agentno);
						agentService.setAgentusername(transAgentStatus.getUsername());
					}
					NettyClients.getInstance().sendAgentEventMessage(agentno, UKDataContext.MessageTypeEnum.NEW.toString(), agentUser);
				}
			}else{
				agentUser = agentUserRepository.findByIdAndOrgi(agentService.getAgentuserid(), super.getOrgi(request));
				if(agentUser!=null){
					agentUser.setAgentno(agentno);	
					agentUserRepository.save(agentUser) ;
				}
			}
			
			if(agentService!=null){
				agentService.setAgentno(agentno);
				if(!StringUtils.isBlank(memo)){
					agentService.setTransmemo(memo);
				}
				agentService.setTrans(true);
				agentService.setTranstime(new Date());
				agentServiceRes.save(agentService) ;
			}
		}
		
    	return request(super.createRequestPageTempletResponse("redirect:/service/current/index.html")) ; 
    }
	
	@RequestMapping("/current/end")
    @Menu(type = "service" , subtype = "current" )
    public ModelAndView end(ModelMap map , HttpServletRequest request , @Valid String id) throws Exception {
		if(!StringUtils.isBlank(id)){
			AgentService agentService = agentServiceRes.findByIdAndOrgi(id, super.getOrgi(request)) ;
			if(agentService!=null){
				User user = super.getUser(request);
				List<AgentUser> agentUser = agentUserRepository.findByUseridAndOrgi(agentService.getUserid(), super.getOrgi(request));
				if(agentUser!=null && agentUser.size() > 0){
					ServiceQuene.deleteAgentUser(agentUser.get(0), user.getOrgi() , UKDataContext.EndByType.AGENT.toString());
				}
				agentService.setStatus(UKDataContext.AgentUserStatusEnum.END.toString());
				agentServiceRes.save(agentService) ;
			}
		}
        return request(super.createRequestPageTempletResponse("redirect:/service/current/index.html"));
    }
	
	@RequestMapping("/current/invite")
    @Menu(type = "service" , subtype = "current" )
    public ModelAndView currentinvite(ModelMap map , HttpServletRequest request , @Valid String id) throws Exception  {
		if(!StringUtils.isBlank(id)){
			AgentService agentService = agentServiceRes.findByIdAndOrgi(id, super.getOrgi(request)) ;
			if(agentService!=null){
				User user = super.getUser(request);
				if(StringUtils.isBlank(agentService.getAgentno())) {
					AiUser aiUser = (AiUser) CacheHelper.getOnlineUserCacheBean().getCacheObject(agentService.getSessionid(), agentService.getOrgi()) ;
					IP ipdata = null ;
					if(aiUser != null ) {
						ipdata = aiUser.getIpdata() ;
						OnlineUserUtils.newRequestMessage(aiUser.getUserid() , aiUser.getUsername(), user.getOrgi(), agentService.getSessionid(), agentService.getAppid() , agentService.getIpaddr(), agentService.getOsname() , agentService.getBrowser() , "" , ipdata!=null ? ipdata : null , agentService.getChannel() , user.getOrgan(), user.getId() , null ,null, agentService.getContactsid(), UKDataContext.ChatInitiatorType.AGENT.toString() , aiUser.getContextid() , null) ;
					}
				}
			}
		}
        return request(super.createRequestPageTempletResponse("redirect:/service/current/index.html"));
    }
	
	
	@RequestMapping("/quene/index")
    @Menu(type = "service" , subtype = "quene" )
    public ModelAndView quene(ModelMap map , HttpServletRequest request) {
		Page<AgentUser> agentUserList = agentUserRes.findByOrgiAndStatus(super.getOrgi(request), UKDataContext.AgentUserStatusEnum.INQUENE.toString() ,new PageRequest(super.getP(request), super.getPs(request), Direction.DESC , "createtime")) ;
		List<String> skillList = new ArrayList<String>();
		for(AgentUser agentUser : agentUserList.getContent()){
			agentUser.setWaittingtime((int) (System.currentTimeMillis() - agentUser.getCreatetime().getTime()));
			if(!StringUtils.isBlank(agentUser.getSkill())){
				skillList.add(agentUser.getSkill()) ;
			}
			
			if(!StringUtils.isBlank(agentUser.getAppid())) {
				SNSAccount snsAccount = SnsAccountUtil.process(agentUser.getAppid() ,agentUser.getOrgi()) ;
				if(snsAccount!=null) {
					agentUser.setSnsname(snsAccount.getName());
				}
			}
		}
		if(skillList.size() > 0){
			List<Organ> organList = organRes.findAll(skillList) ;
			for(AgentUser agentUser : agentUserList.getContent()){
				if(!StringUtils.isBlank(agentUser.getSkill())){
					for(Organ organ : organList){
						if(agentUser.getSkill().equals(organ.getId())){
							agentUser.setSkillname(organ.getName());
							break ;
						}
					}
				}
			}
		}
		map.put("agentUserList", agentUserList) ;
        return request(super.createAppsTempletResponse("/apps/service/quene/index"));
    }
	
	@RequestMapping("/quene/clean")
    @Menu(type = "service" , subtype = "queneclean" )
    public ModelAndView clean(ModelMap map , HttpServletRequest request ,@Valid String id) {
		AgentUser agentUser = agentUserRes.findByIdAndOrgi(id, super.getOrgi(request)) ;
		if(agentUser!=null && agentUser.getStatus().equals(UKDataContext.AgentUserStatusEnum.INQUENE.toString())){
			agentUser.setAgent(null);
			agentUser.setSkill(null);
			agentUserRes.save(agentUser) ;
			CacheHelper.getAgentUserCacheBean().put(agentUser.getUserid(), agentUser, super.getOrgi(request));
			ServiceQuene.allotAgent(agentUser, super.getOrgi(request)) ;
		}
        return request(super.createRequestPageTempletResponse("redirect:/service/quene/index.html"));
    }
	
	@RequestMapping("/quene/invite")
    @Menu(type = "service" , subtype = "invite" )
    public ModelAndView invite(ModelMap map , HttpServletRequest request ,@Valid String id) throws Exception {
		AgentUser agentUser = agentUserRes.findByIdAndOrgi(id, super.getOrgi(request)) ;
		if(agentUser!=null && agentUser.getStatus().equals(UKDataContext.AgentUserStatusEnum.INQUENE.toString())){
			ServiceQuene.allotAgentForInvite(super.getUser(request).getId() , agentUser, super.getOrgi(request)) ;
		}
        return request(super.createRequestPageTempletResponse("redirect:/service/quene/index.html"));
    }
	
	@RequestMapping("/agent/index")
    @Menu(type = "service" , subtype = "onlineagent" )
    public ModelAndView agent(ModelMap map , HttpServletRequest request) {
		List<AgentStatus> agentStatusList = agentStatusRepository.findByOrgi(super.getOrgi(request)) ;
		for(int i=0 ; i<agentStatusList.size() ; ){
			AgentStatus agentStatus = agentStatusList.get(i) ;
			if(CacheHelper.getAgentStatusCacheBean().getCacheObject(agentStatus.getAgentno(), super.getOrgi(request))==null) {
				agentStatusRepository.delete(agentStatus); 
				agentStatusList.remove(i) ;
				continue ;
			}else{
				AgentStatus temp = (AgentStatus) CacheHelper.getAgentStatusCacheBean().getCacheObject(agentStatus.getAgentno(), super.getOrgi(request)) ;
				agentStatusList.set(i, temp) ;	
			}
			i++ ;
		}
		List<String> skillList = new ArrayList<String>();
		for(AgentStatus agentStatus : agentStatusList){
			if(!StringUtils.isBlank(agentStatus.getSkill())){
				skillList.add(agentStatus.getSkill()) ;
			}
		}
		if(skillList.size() > 0){
			List<Organ> organList = organRes.findAll(skillList) ;
			for(AgentStatus agentStatus : agentStatusList){
				if(!StringUtils.isBlank(agentStatus.getSkill())){
					for(Organ organ : organList){
						if(agentStatus.getSkill().equals(organ.getId())){
							agentStatus.setSkillname(organ.getName());
							break ;
						}
					}
				}
			}
		}
		map.put("agentStatusList", agentStatusList) ;
		map.put("sessionConfig", ServiceQuene.initSessionConfig(super.getOrgi(request))) ;
		 
        return request(super.createAppsTempletResponse("/apps/service/agent/index"));
    }
	
	@RequestMapping("/agent/offline")
    @Menu(type = "service" , subtype = "offline" )
    public ModelAndView offline(ModelMap map , HttpServletRequest request , @Valid String id) {
		
		AgentStatus agentStatus = agentStatusRepository.findByIdAndOrgi(id, super.getOrgi(request));
		if(agentStatus!=null){
			agentStatusRepository.delete(agentStatus);
			
			CacheHelper.getAgentStatusCacheBean().delete(agentStatus.getAgentno(), super.getOrgi(request));;
	    	ServiceQuene.publishMessage(super.getOrgi(request) , "agent" , "offline" , super.getUser(request).getId());
		}
		
        return request(super.createRequestPageTempletResponse("redirect:/service/agent/index.html"));
    }
	/**
	 * 非管理员坐席
	 * @param map
	 * @param request
	 * @return
	 */
	@RequestMapping("/user/index")
    @Menu(type = "service" , subtype = "userlist" )
    public ModelAndView user(ModelMap map , HttpServletRequest request) {
		Page<User> userList = null;
		if(super.isTenantshare()) {
			List<String> organIdList = new ArrayList<>();
			List<OrgiSkillRel> orgiSkillRelList = orgiSkillRelService.findByOrgi(super.getOrgi(request)) ;
			if(!orgiSkillRelList.isEmpty()) {
				for(OrgiSkillRel rel:orgiSkillRelList) {
					organIdList.add(rel.getSkillid());
				}
			}
			userList=userRes.findByOrganInAndAgentAndDatastatus(organIdList,true,false,new PageRequest(super.getP(request), super.getPs(request), Direction.DESC , "createtime"));
		}else {
			userList=userRes.findByOrgiAndAgentAndDatastatus(super.getOrgi(request), true,false,  new PageRequest(super.getP(request), super.getPs(request), Direction.DESC , "createtime")) ;
		}
		for(User user : userList.getContent()){
			if(CacheHelper.getAgentStatusCacheBean().getCacheObject(user.getId(), super.getOrgi(request))!=null){
				user.setOnline(true);
			}
		}
		map.put("userList", userList) ;
        return request(super.createAppsTempletResponse("/apps/service/user/index"));
    }
	/**
	 * 管理员坐席
	 * @param map
	 * @param request
	 * @return
	 */
	@RequestMapping("/adminagent/index")
    @Menu(type = "service" , subtype = "adminagentlist" )
    public ModelAndView adminagent(ModelMap map , HttpServletRequest request) {
		Page<User> userList = userRes.findByOrgidAndAgentAndDatastatusAndUsertype(super.getOrgid(request), true,false,"0",  new PageRequest(super.getP(request), super.getPs(request), Direction.DESC , "createtime")) ;
		for(User user : userList.getContent()){
			if(CacheHelper.getAgentStatusCacheBean().getCacheObject(user.getId(), super.getOrgi(request))!=null){
				user.setOnline(true);
			}
		}
		map.put("userList", userList) ;
        return request(super.createAppsTempletResponse("/apps/service/adminagent/index"));
    }
	@RequestMapping("/leavemsg/index")
    @Menu(type = "service" , subtype = "leavemsg" )
    public ModelAndView leavemsg(ModelMap map , HttpServletRequest request) {
		Page<LeaveMsg> leaveMsgList = leaveMsgRes.findByOrgi(super.getOrgi(request),new PageRequest(super.getP(request), super.getPs(request), Direction.DESC , "createtime")) ;
		map.put("leaveMsgList", leaveMsgList) ;
        return request(super.createAppsTempletResponse("/apps/service/leavemsg/index"));
    }
	
	@RequestMapping("/leavemsg/delete")
    @Menu(type = "service" , subtype = "leavemsg" )
    public ModelAndView leavemsg(ModelMap map , HttpServletRequest request , @Valid String id) {
		if(!StringUtils.isBlank(id)){
			leaveMsgRes.delete(id);
		}
		return request(super.createRequestPageTempletResponse("redirect:/service/leavemsg/index.html"));
    }
	
	
	@RequestMapping("/expids")
	@Menu(type = "callcenter", subtype = "callcenter")
	public void expids(ModelMap map, HttpServletRequest request, HttpServletResponse response, @Valid String[] ids)
			throws IOException {
		if (ids != null && ids.length > 0) {
			Iterable<AgentService> agentServiceList = agentServiceRes.findAll(Arrays.asList(ids));
			MetadataTable table = metadataRes.findByTablenameIgnoreCase("uk_agentservice");
			List<Map<String, Object>> values = new ArrayList<Map<String, Object>>();
			for (AgentService agentService : agentServiceList) {
				values.add(UKTools.transBean2Map(agentService));
			}

			response.setHeader("content-disposition", "attachment;filename=UCKeFu-AgentService-History-"
					+ new SimpleDateFormat("yyyy-MM-dd").format(new Date()) + ".xls");

			ExcelExporterProcess excelProcess = new ExcelExporterProcess(values, table, response.getOutputStream());
			excelProcess.process();
		}

		return;
	}

	@RequestMapping("/expall")
	@Menu(type = "callcenter", subtype = "callcenter")
	public void expall(ModelMap map, HttpServletRequest request, HttpServletResponse response) throws IOException {
		Iterable<AgentService> agentServiceList = agentServiceRes.findByOrgi(super.getOrgi(request), new PageRequest(0, 10000));

		MetadataTable table = metadataRes.findByTablenameIgnoreCase("uk_agentservice");
		List<Map<String, Object>> values = new ArrayList<Map<String, Object>>();
		for (AgentService agentService : agentServiceList) {
			values.add(UKTools.transBean2Map(agentService));
		}

		response.setHeader("content-disposition", "attachment;filename=UCKeFu-AgentService-History-"
				+ new SimpleDateFormat("yyyy-MM-dd").format(new Date()) + ".xls");

		ExcelExporterProcess excelProcess = new ExcelExporterProcess(values, table, response.getOutputStream());
		excelProcess.process();
		return;
	}

	@RequestMapping("/expsearch")
	@Menu(type = "callcenter", subtype = "callcenter")
	public void expall(ModelMap map , HttpServletResponse response , HttpServletRequest request ,final String username,final String channel ,final String servicetype,final String skill,final String agent,final String servicetimetype,final String begin,final String end , final String sbegin,final String send) throws IOException {
		final String orgi = super.getOrgi(request);
		Page<AgentService> page = agentServiceRes.findAll(new Specification<AgentService>(){
			@Override
			public Predicate toPredicate(Root<AgentService> root, CriteriaQuery<?> query,CriteriaBuilder cb) {
				List<Predicate> list = new ArrayList<Predicate>();  
				if(!StringUtils.isBlank(username)) {
					list.add(cb.equal(root.get("username").as(String.class), username)) ;
				}
				
				list.add(cb.equal(root.get("orgi").as(String.class), orgi)) ;
				
				if(!StringUtils.isBlank(channel)) {
					list.add(cb.equal(root.get("channel").as(String.class), channel)) ;
				}
				if(!StringUtils.isBlank(agent)) {
					list.add(cb.equal(root.get("agentno").as(String.class), agent)) ;
				}
				if(!StringUtils.isBlank(skill)) {
					list.add(cb.equal(root.get("skill").as(String.class), skill)) ;
				}
				try {
					if(!StringUtils.isBlank(begin) && begin.matches("[\\d]{4}-[\\d]{2}-[\\d]{2} [\\d]{2}:[\\d]{2}:[\\d]{2}")){
						list.add(cb.greaterThanOrEqualTo(root.get("logindate").as(Date.class), UKTools.dateFormate.parse(begin))) ;
					}
					if(!StringUtils.isBlank(end) && end.matches("[\\d]{4}-[\\d]{2}-[\\d]{2} [\\d]{2}:[\\d]{2}:[\\d]{2}")){
						list.add(cb.lessThanOrEqualTo(root.get("logindate").as(Date.class), UKTools.dateFormate.parse(end))) ;
					}
					
					if(!StringUtils.isBlank(sbegin) && sbegin.matches("[\\d]{4}-[\\d]{2}-[\\d]{2} [\\d]{2}:[\\d]{2}:[\\d]{2}")){
						list.add(cb.greaterThanOrEqualTo(root.get("servicetime").as(Date.class), UKTools.dateFormate.parse(sbegin))) ;
					}
					if(!StringUtils.isBlank(send) && send.matches("[\\d]{4}-[\\d]{2}-[\\d]{2} [\\d]{2}:[\\d]{2}:[\\d]{2}")){
						list.add(cb.lessThanOrEqualTo(root.get("servicetime").as(Date.class), UKTools.dateFormate.parse(send))) ;
					}
				} catch (ParseException e) {
					e.printStackTrace();
				}
				
				Predicate[] p = new Predicate[list.size()];  
			    return cb.and(list.toArray(p));   
			}
		},new PageRequest(super.getP(request),10000, Direction.DESC , "createtime")) ;

		MetadataTable table = metadataRes.findByTablenameIgnoreCase("uk_agentservice");
		List<Map<String, Object>> values = new ArrayList<Map<String, Object>>();
		for (AgentService agentService : page.getContent()) {
			values.add(UKTools.transBean2Map(agentService));
		}

		response.setHeader("content-disposition", "attachment;filename=UCKeFu-AgentService-History-"
				+ new SimpleDateFormat("yyyy-MM-dd").format(new Date()) + ".xls");

		ExcelExporterProcess excelProcess = new ExcelExporterProcess(values, table, response.getOutputStream());
		excelProcess.process();

		return;
	}
	/**
	 * 	机器人历史会话 - 首页
	 */
	@RequestMapping("/aihistory/index")
    @Menu(type = "service" , subtype = "aihistory" )
    public ModelAndView xiaoeHis(ModelMap map , HttpServletRequest request ,final String username,final String channel ,final String servicetype,final String skill,final String agent,final String servicetimetype,final String begin,final String end , final String sbegin,final String send) {
		final String orgi = super.getOrgi(request);
		Page<AgentService> page = agentServiceRes.findAll(new Specification<AgentService>(){
			@Override
			public Predicate toPredicate(Root<AgentService> root, CriteriaQuery<?> query,CriteriaBuilder cb) {
				List<Predicate> list = new ArrayList<Predicate>();  
				list.add(cb.equal(root.get("aiservice").as(boolean.class), true)) ;
				if(!StringUtils.isBlank(username)) {
					list.add(cb.equal(root.get("username").as(String.class), username)) ;
				}
				
				list.add(cb.equal(root.get("orgi").as(String.class), orgi)) ;
				
				if(!StringUtils.isBlank(channel)) {
					list.add(cb.equal(root.get("channel").as(String.class), channel)) ;
				}
				if(!StringUtils.isBlank(agent)) {
					list.add(cb.equal(root.get("agentno").as(String.class), agent)) ;
				}
				if(!StringUtils.isBlank(skill)) {
					list.add(cb.equal(root.get("skill").as(String.class), skill)) ;
				}
				try {
					if(!StringUtils.isBlank(begin) && begin.matches("[\\d]{4}-[\\d]{2}-[\\d]{2} [\\d]{2}:[\\d]{2}:[\\d]{2}")){
						list.add(cb.greaterThanOrEqualTo(root.get("logindate").as(Date.class), UKTools.dateFormate.parse(begin))) ;
					}
					if(!StringUtils.isBlank(end) && end.matches("[\\d]{4}-[\\d]{2}-[\\d]{2} [\\d]{2}:[\\d]{2}:[\\d]{2}")){
						list.add(cb.lessThanOrEqualTo(root.get("logindate").as(Date.class), UKTools.dateFormate.parse(end))) ;
					}
					
					if(!StringUtils.isBlank(sbegin) && sbegin.matches("[\\d]{4}-[\\d]{2}-[\\d]{2} [\\d]{2}:[\\d]{2}:[\\d]{2}")){
						list.add(cb.greaterThanOrEqualTo(root.get("servicetime").as(Date.class), UKTools.dateFormate.parse(sbegin))) ;
					}
					if(!StringUtils.isBlank(send) && send.matches("[\\d]{4}-[\\d]{2}-[\\d]{2} [\\d]{2}:[\\d]{2}:[\\d]{2}")){
						list.add(cb.lessThanOrEqualTo(root.get("servicetime").as(Date.class), UKTools.dateFormate.parse(send))) ;
					}
				} catch (ParseException e) {
					e.printStackTrace();
				}
				
				Predicate[] p = new Predicate[list.size()];  
			    return cb.and(list.toArray(p));   
			}
		},new PageRequest(super.getP(request), super.getPs(request), Direction.DESC , "createtime")) ;
		map.put("agentServiceList", page) ;
		map.put("username", username) ;
		map.put("channel", channel) ;
		map.put("servicetype", servicetype) ;
		map.put("servicetimetype", servicetimetype) ;
		map.put("agent", agent);
		map.put("skill", skill);
		map.put("begin", begin) ;
		map.put("end", end) ;
		map.put("sbegin", sbegin) ;
		map.put("send", send) ;
		map.put("organlist",organ.findByOrgi(super.getOrgi(request)));
		map.put("userlist",user.findByOrgiAndDatastatus(super.getOrgi(request), false));
		List<ServiceAi> aiList = this.serviceAiRes.findByOrgi(super.getOrgi(request));
		map.addAttribute("aiList", aiList);
        return request(super.createAppsTempletResponse("/apps/service/aihistory/index"));
    }
	/**
	 * 	导出全部机器人会话历史
	 */
	@RequestMapping("/aihistory/expall")
	@Menu(type = "callcenter", subtype = "callcenter")
	public void aiexpall(ModelMap map, HttpServletRequest request, HttpServletResponse response) throws IOException {
		//Iterable<AgentService> agentServiceList = agentServiceRes.findByOrgi(super.getOrgi(request), new PageRequest(0, 10000));
		
		final String orgi = super.getOrgi(request);
		Iterable<AgentService> agentServiceList = agentServiceRes.findAll(new Specification<AgentService>(){
			@Override
			public Predicate toPredicate(Root<AgentService> root, CriteriaQuery<?> query,CriteriaBuilder cb) {
				List<Predicate> list = new ArrayList<Predicate>();  
				list.add(cb.equal(root.get("aiservice").as(boolean.class), true)) ;
				list.add(cb.equal(root.get("orgi").as(String.class), orgi)) ;
				Predicate[] p = new Predicate[list.size()];  
			    return cb.and(list.toArray(p));   
			}
		},new PageRequest(0, 10000, Direction.DESC , "createtime")) ;
		
		MetadataTable table = metadataRes.findByTablenameIgnoreCase("uk_agentservice");
		List<Map<String, Object>> values = new ArrayList<Map<String, Object>>();
		for (AgentService agentService : agentServiceList) {
			values.add(UKTools.transBean2Map(agentService));
		}

		response.setHeader("content-disposition", "attachment;filename=UCKeFu-AgentService-History-"
				+ new SimpleDateFormat("yyyy-MM-dd").format(new Date()) + ".xls");

		ExcelExporterProcess excelProcess = new ExcelExporterProcess(values, table, response.getOutputStream());
		excelProcess.process();
		return;
	}
	/**
	 * 	导出当前搜索出来的，机器人会话
	 */
	@RequestMapping("/aihistory/expsearch")
	@Menu(type = "callcenter", subtype = "callcenter")
	public void aiExpsearch(ModelMap map , HttpServletResponse response , HttpServletRequest request ,final String username,final String channel ,final String servicetype,final String skill,final String agent,final String servicetimetype,final String begin,final String end , final String sbegin,final String send) throws IOException {
		final String orgi = super.getOrgi(request);
		Page<AgentService> page = agentServiceRes.findAll(new Specification<AgentService>(){
			@Override
			public Predicate toPredicate(Root<AgentService> root, CriteriaQuery<?> query,CriteriaBuilder cb) {
				List<Predicate> list = new ArrayList<Predicate>();  
				list.add(cb.equal(root.get("aiservice").as(boolean.class), true)) ;
				list.add(cb.equal(root.get("orgi").as(String.class), orgi)) ;
				if(!StringUtils.isBlank(username)) {
					list.add(cb.equal(root.get("username").as(String.class), username)) ;
				}
				if(!StringUtils.isBlank(channel)) {
					list.add(cb.equal(root.get("channel").as(String.class), channel)) ;
				}
				if(!StringUtils.isBlank(agent)) {
					list.add(cb.equal(root.get("agentno").as(String.class), agent)) ;
				}
				if(!StringUtils.isBlank(skill)) {
					list.add(cb.equal(root.get("skill").as(String.class), skill)) ;
				}
				try {
					if(!StringUtils.isBlank(begin) && begin.matches("[\\d]{4}-[\\d]{2}-[\\d]{2} [\\d]{2}:[\\d]{2}:[\\d]{2}")){
						list.add(cb.greaterThanOrEqualTo(root.get("logindate").as(Date.class), UKTools.dateFormate.parse(begin))) ;
					}
					if(!StringUtils.isBlank(end) && end.matches("[\\d]{4}-[\\d]{2}-[\\d]{2} [\\d]{2}:[\\d]{2}:[\\d]{2}")){
						list.add(cb.lessThanOrEqualTo(root.get("logindate").as(Date.class), UKTools.dateFormate.parse(end))) ;
					}
					
					if(!StringUtils.isBlank(sbegin) && sbegin.matches("[\\d]{4}-[\\d]{2}-[\\d]{2} [\\d]{2}:[\\d]{2}:[\\d]{2}")){
						list.add(cb.greaterThanOrEqualTo(root.get("servicetime").as(Date.class), UKTools.dateFormate.parse(sbegin))) ;
					}
					if(!StringUtils.isBlank(send) && send.matches("[\\d]{4}-[\\d]{2}-[\\d]{2} [\\d]{2}:[\\d]{2}:[\\d]{2}")){
						list.add(cb.lessThanOrEqualTo(root.get("servicetime").as(Date.class), UKTools.dateFormate.parse(send))) ;
					}
				} catch (ParseException e) {
					e.printStackTrace();
				}
				
				Predicate[] p = new Predicate[list.size()];  
			    return cb.and(list.toArray(p));   
			}
		},new PageRequest(0,10000, Direction.DESC , "createtime")) ;

		MetadataTable table = metadataRes.findByTablenameIgnoreCase("uk_agentservice");
		List<Map<String, Object>> values = new ArrayList<Map<String, Object>>();
		for (AgentService agentService : page.getContent()) {
			values.add(UKTools.transBean2Map(agentService));
		}

		response.setHeader("content-disposition", "attachment;filename=UCKeFu-AgentService-History-"
				+ new SimpleDateFormat("yyyy-MM-dd").format(new Date()) + ".xls");

		ExcelExporterProcess excelProcess = new ExcelExporterProcess(values, table, response.getOutputStream());
		excelProcess.process();

		return;
	}
	
	/*
	 * 导出全部机器人会话内容
	 */
	@RequestMapping("/chatmessage/expall")
	@Menu(type = "callcenter", subtype = "callcenter")
	public void chatExpall(ModelMap map, HttpServletRequest request, HttpServletResponse response) throws IOException {
		
		Page<ChatMessage> chatList = chatMessageRes.findByAichatAndOrgi(true,super.getOrgi(request), new PageRequest(0, 10000 , Direction.ASC , "agentserviceid" , "createtime"));
		MetadataTable table = metadataRes.findByTablenameIgnoreCase("uk_chat_message");
		List<Map<String, Object>> values = new ArrayList<Map<String, Object>>();
		for (ChatMessage chatmessage : chatList) {
			values.add(UKTools.transBean2Map(chatmessage));
		}
		response.setHeader("content-disposition", "attachment;filename=UCKeFu-ChatMessage-Content-"
				+ new SimpleDateFormat("yyyy-MM-dd").format(new Date()) + ".xls");
		ExcelExporterProcess excelProcess = new ExcelExporterProcess(values, table, response.getOutputStream());
		excelProcess.process();
		return;
	}
	/*
	 * 导出机器人会话内容搜索结果
	 */
	@RequestMapping("/chatmessage/expsearch")
	@Menu(type = "callcenter", subtype = "callcenter")
	public void aiExpall(ModelMap map , HttpServletResponse response , HttpServletRequest request ,final String username,final String channel ,final String servicetype,final String skill,final String agent,final String servicetimetype,final String begin,final String end , final String sbegin,final String send) throws IOException {
		final String orgi = super.getOrgi(request);
		
		Page<AgentService> page = agentServiceRes.findAll(new Specification<AgentService>(){
			@Override
			public Predicate toPredicate(Root<AgentService> root, CriteriaQuery<?> query,CriteriaBuilder cb) {
				List<Predicate> list = new ArrayList<Predicate>(); 
				list.add(cb.equal(root.get("aiservice").as(boolean.class), true)) ;
				if(!StringUtils.isBlank(username)) {
					list.add(cb.equal(root.get("username").as(String.class), username)) ;
				}
				
				list.add(cb.equal(root.get("orgi").as(String.class), orgi)) ;
				
				if(!StringUtils.isBlank(channel)) {
					list.add(cb.equal(root.get("channel").as(String.class), channel)) ;
				}
				if(!StringUtils.isBlank(agent)) {
					list.add(cb.equal(root.get("agentno").as(String.class), agent)) ;
				}
				if(!StringUtils.isBlank(skill)) {
					list.add(cb.equal(root.get("skill").as(String.class), skill)) ;
				}
				try {
					if(!StringUtils.isBlank(begin) && begin.matches("[\\d]{4}-[\\d]{2}-[\\d]{2} [\\d]{2}:[\\d]{2}:[\\d]{2}")){
						list.add(cb.greaterThanOrEqualTo(root.get("logindate").as(Date.class), UKTools.dateFormate.parse(begin))) ;
					}
					if(!StringUtils.isBlank(end) && end.matches("[\\d]{4}-[\\d]{2}-[\\d]{2} [\\d]{2}:[\\d]{2}:[\\d]{2}")){
						list.add(cb.lessThanOrEqualTo(root.get("logindate").as(Date.class), UKTools.dateFormate.parse(end))) ;
					}
					
					if(!StringUtils.isBlank(sbegin) && sbegin.matches("[\\d]{4}-[\\d]{2}-[\\d]{2} [\\d]{2}:[\\d]{2}:[\\d]{2}")){
						list.add(cb.greaterThanOrEqualTo(root.get("servicetime").as(Date.class), UKTools.dateFormate.parse(sbegin))) ;
					}
					if(!StringUtils.isBlank(send) && send.matches("[\\d]{4}-[\\d]{2}-[\\d]{2} [\\d]{2}:[\\d]{2}:[\\d]{2}")){
						list.add(cb.lessThanOrEqualTo(root.get("servicetime").as(Date.class), UKTools.dateFormate.parse(send))) ;
					}
					
				} catch (ParseException e) {
					e.printStackTrace();
				}
				
				Predicate[] p = new Predicate[list.size()];  
			    return cb.and(list.toArray(p));   
			}
		},new PageRequest(super.getP(request), 10000, Direction.DESC , "createtime")) ;
		List<ChatMessage> chatAllList = new ArrayList<ChatMessage>();
		for(AgentService agentService:page.getContent()) {
			Page<ChatMessage> chatList = chatMessageRes.findByAgentserviceidAndOrgi(agentService.getId() , orgi , new PageRequest(0, 10000, Direction.ASC , "createtime"));
			if(chatList.getTotalElements() > 0) {
				chatAllList.addAll(chatList.getContent());
			}
		}
		this.getAiChatMessageExcel(response, chatAllList);    
		return;
	}
	@RequestMapping("/chatmessage/expids")
	@Menu(type = "callcenter", subtype = "callcenter")
	public void aiExpids(ModelMap map, HttpServletRequest request, HttpServletResponse response, @Valid String[] ids)
			throws IOException {
		if (ids != null && ids.length > 0) {
			List<ChatMessage> chatAllList = new ArrayList<ChatMessage>();
			for(String id : ids) {
				Page<ChatMessage> chatList = chatMessageRes.findByAgentserviceidAndOrgi(id , super.getOrgi(request), new PageRequest(0, 10000, Direction.ASC , "createtime"));
				if(chatList.getTotalElements() > 0) {
					chatAllList.addAll(chatList.getContent());
				}
			}
			this.getAiChatMessageExcel(response, chatAllList);
		}
		return;
	}
	public void getAiChatMessageExcel( HttpServletResponse response, List<ChatMessage> chatList) throws IOException {
		MetadataTable table = metadataRes.findByTablenameIgnoreCase("uk_chat_message");
		List<Map<String, Object>> values = new ArrayList<Map<String, Object>>();
		for (ChatMessage chatmessage : chatList) {
			values.add(UKTools.transBean2Map(chatmessage));
		}

		response.setHeader("content-disposition", "attachment;filename=UCKeFu-ChatMessage-Content-"
				+ new SimpleDateFormat("yyyy-MM-dd").format(new Date()) + ".xls");

		ExcelExporterProcess excelProcess = new ExcelExporterProcess(values, table, response.getOutputStream());
		excelProcess.process();
	}
	
	
	@RequestMapping("/agent/agent")
	@Menu(type = "apps", subtype = "agent")
	public ModelAndView agent(ModelMap map , HttpServletRequest request ,HttpServletResponse response , @Valid String sort,@Valid String id) throws IOException, TemplateException {
		map.addAttribute("isServiceAgent", "1");
		ModelAndView view = request(super.createAppsTempletResponse("/apps/service/agent/agent")) ; 
		User user = userRes.findById(id) ;
		if (user != null) {
			Sort defaultSort = null ;
			if(StringUtils.isBlank(sort)){
				Cookie[] cookies = request.getCookies();//这样便可以获取一个cookie数组
				if(cookies!=null){
					for(Cookie cookie : cookies){
						if(cookie.getName().equals("sort")){
							sort = cookie.getValue() ;break ; 
						}
					}
				}
			}
			if(!StringUtils.isBlank(sort)){
				List<Order> list = new ArrayList<Order>();
				if(sort.equals("lastmessage")){
					list.add(new Order(Direction.DESC,"status")) ;
					list.add(new Order(Direction.DESC,"lastmessage")) ;
				}else if(sort.equals("logintime")){
					list.add(new Order(Direction.DESC,"status")) ;
					list.add(new Order(Direction.DESC,"createtime")) ;
				}else if(sort.equals("default")){
					defaultSort = new Sort(Direction.DESC,"status") ;
					Cookie name = new Cookie("sort",null);
					name.setMaxAge(0);
					response.addCookie(name);
				}
				if(list.size() > 0){
					defaultSort = new Sort(list) ;
					Cookie name = new Cookie("sort",sort);
					name.setMaxAge(60*60*24*365);
					response.addCookie(name);
					map.addAttribute("sort", sort) ;
				}
			}else{
				defaultSort = new Sort(Direction.DESC,"status") ;
			}
			List<AgentUser> agentUserList = agentUserRepository.findByAgentnoAndOrgi(user.getId() , super.getOrgi(request) , defaultSort);
			view.addObject("agentUserList", agentUserList) ;
			
			SessionConfig sessionConfig = ServiceQuene.initSessionConfig(super.getOrgi(request)) ;
			
			view.addObject("sessionConfig", sessionConfig) ;
			
			if(agentUserList.size() > 0){
				AgentUser agentUser = agentUserList.get(0) ;
				agentUser = (AgentUser) agentUserList.get(0);
				view.addObject("curagentuser", agentUser);
				view.addObject("inviteData",  OnlineUserUtils.cousult(agentUser.getAppid(), agentUser.getOrgi(), inviteRepository));
				if(!StringUtils.isBlank(agentUser.getAgentserviceid())){
					/*
					 * AgentServiceSummary summary =
					 * this.serviceSummaryRes.findByAgentserviceidAndOrgi(agentUser.
					 * getAgentserviceid(), super.getOrgi(request)) ; if(summary!=null){
					 * view.addObject("summary", summary) ; }
					 */
					Page<AgentServiceSummary> summaryList = this.serviceSummaryRes.findByOrgiAndUserid(super.getOrgi(request), agentUser.getUserid() , new PageRequest(0, super.getPs(request), Sort.Direction.DESC, new String[] { "createtime" }));
					//Page<AgentServiceSummary> summaryList = this.serviceSummaryRes.findByOrgiAndAgentserviceid(super.getOrgi(request), agentUser.getAgentserviceid(),new PageRequest(0, super.getPs(request), Sort.Direction.DESC, new String[] { "createtime" }));
					view.addObject("summaryList", summaryList) ;
				}
				
				if(sessionConfig.isOtherquickplay() && !StringUtils.isBlank(sessionConfig.getOqrsearchurl())) {
					view.addObject("topicList", OnlineUserUtils.search(null, super.getOrgi(request), super.getUser(request) , agentUser.getAppid())) ;
				}else if(!sessionConfig.isOtherquickplay()){//ekm知识库
					if(UKDataContext.model.get("ekm")!=null ){
						EkmDataInterface dataExchange = (EkmDataInterface) UKDataContext.getContext().getBean("ekm") ;
						Page<EkmKnowledgeMaster> knowledgeList = dataExchange.findByOrgi(map, request, super.getOrgi(request), super.getUser(request), new PageRequest(super.getP(request), 10000)) ;
						
						view.addObject("topicList", knowledgeList.getContent()) ;
					}
				}
				
				view.addObject("agentUserMessageList", this.chatMessageRepository.findByUsessionAndOrgi(agentUser.getUserid() , super.getOrgi(request), new PageRequest(0, 20, Direction.DESC , "updatetime")));
				AgentService agentService = null ;
				if(!StringUtils.isBlank(agentUser.getAgentserviceid())){
					agentService = this.agentServiceRepository.findOne(agentUser.getAgentserviceid()) ;
					view.addObject("curAgentService", agentService) ;
					
					
					if(agentService!=null){
						/**
						 * 获取关联数据
						 */
						processRelaData(request, agentService, map);
						map.addAttribute("dataid", agentService.getId()) ;
					}
				}
				
				if(UKDataContext.ChannelTypeEnum.WEIXIN.toString().equals(agentUser.getChannel())){
					List<WeiXinUser> weiXinUserList = weiXinUserRes.findByOpenidAndSnsidAndOrgi(agentUser.getUserid(),agentUser.getAppid(), super.getOrgi(request)) ;
					if(weiXinUserList.size() > 0){
						WeiXinUser weiXinUser = weiXinUserList.get(0) ;
						view.addObject("weiXinUser",weiXinUser);
					}
				}else if(UKDataContext.ChannelTypeEnum.WEBIM.toString().equals(agentUser.getChannel())){
					List<OnlineUser> onlineUserList = this.onlineUserESRes.findByUseridAndOrgi(agentUser.getUserid(), super.getOrgi(request)) ;
					if(onlineUserList.size()  > 0){
						OnlineUser onlineUser = onlineUserList.get(0) ;
						if(UKDataContext.OnlineUserOperatorStatus.OFFLINE.toString().equals(onlineUser.getStatus())){
							onlineUser.setBetweentime((int) (onlineUser.getUpdatetime().getTime() - onlineUser.getLogintime().getTime()));
						}else{
							onlineUser.setBetweentime((int) (System.currentTimeMillis() - onlineUser.getLogintime().getTime()));
						}
						view.addObject("onlineUser",onlineUser);
					}
				}else if(UKDataContext.ChannelTypeEnum.PHONE.toString().equals(agentUser.getChannel())){
					if(agentService!=null && !StringUtils.isBlank(agentService.getOwner())) {
						StatusEvent statusEvent = this.statusEventRes.findById(agentService.getOwner()) ;
						if(statusEvent!=null){
							if(!StringUtils.isBlank(statusEvent.getHostid())) {
								PbxHost pbxHost = pbxHostRes.findById(statusEvent.getHostid()) ;
								view.addObject("pbxHost",pbxHost);
							}
							view.addObject("statusEvent",statusEvent);
						}
					}
				}
				
				view.addObject("serviceCount", Integer
						.valueOf(this.agentServiceRepository
								.countByUseridAndOrgiAndStatus(agentUser
										.getUserid(), super.getOrgi(request),
										UKDataContext.AgentUserStatusEnum.END.toString())));
				
				view.addObject("tags", tagRes.findByOrgiAndTagtype(super.getOrgi(request) , UKDataContext.ModelType.USER.toString())) ;
				view.addObject("tagRelationList", tagRelationRes.findByUserid(agentUser.getUserid())) ;
				view.addObject("quickReplyList", quickReplyRes.findByOrgiAndCreater(super.getOrgi(request) , super.getUser(request).getId() , null)) ;
				List<QuickType> quickTypeList = quickTypeRes.findByOrgiAndQuicktype(super.getOrgi(request), UKDataContext.QuickTypeEnum.PUB.toString()) ;
				List<QuickType> priQuickTypeList = quickTypeRes.findByOrgiAndQuicktypeAndCreater(super.getOrgi(request), UKDataContext.QuickTypeEnum.PRI.toString(), super.getUser(request).getId()) ; 
				quickTypeList.addAll(priQuickTypeList) ;
				view.addObject("pubQuickTypeList", quickTypeList) ;
				map.addAttribute("tagsSummary", tagRes.findByOrgiAndTagtype(super.getOrgi(request) , UKDataContext.ModelType.SUMMARY.toString())) ;
				//文字客服
				SysDic sysDic = sysDicRes.findByCode("sessionWords");
				if(agentService != null &&sysDic != null){
					List<SessionType> sessionTypeList = sessionTypeRes.findByOrgiAndCtype(super.getOrgi(request), sysDic.getId());
					for(SessionType  ses : sessionTypeList){
						if(!StringUtils.isBlank(agentService.getSessiontype()) && ses.getId().equals(agentService.getSessiontype())){
							map.addAttribute("agentSessionType", ses.getName());
						}
					}
				}
				
			}
		}
		return view ;
	}
	
	private void processRelaData(HttpServletRequest request , AgentService agentService , ModelMap map){
		map.addAttribute("aiList",serviceAiRes.findByOrgi(super.getOrgi(request))) ;
		Page<AgentService> agentServiceList = agentServiceRepository.findByUseridAndOrgiAndStatus(agentService.getUserid() , super.getOrgi(request), UKDataContext.AgentUserStatusEnum.END.toString() , new PageRequest(0, 20, Sort.Direction.DESC, new String[] { "endtime" })) ; 
		map.addAttribute("agentServiceList", agentServiceList.getContent()) ;
		if(!StringUtils.isBlank(agentService.getAppid())){
			map.addAttribute("snsAccount", snsAccountRes.findBySnsidAndOrgi(agentService.getAppid(), super.getOrgi(request))  ); 
		}
		List<AgentUserContacts> relaList = agentUserContactsRes.findByUseridAndOrgi(agentService.getUserid(), agentService.getOrgi()) ;
		if(relaList.size() > 0){
			AgentUserContacts agentUserContacts = relaList.get(0) ;
			if(UKDataContext.model.get("contacts")!=null && !StringUtils.isBlank(agentUserContacts.getContactsid())){
				DataExchangeInterface dataExchange = (DataExchangeInterface) UKDataContext.getContext().getBean("contacts") ;
				if(dataExchange!=null){
					map.addAttribute("contacts", dataExchange.getDataByIdAndOrgi(agentUserContacts.getContactsid(), super.getOrgi(request))) ;
				}
			}
			if(UKDataContext.model.get("workorders")!=null && !StringUtils.isBlank(agentUserContacts.getContactsid())){
				DataExchangeInterface dataExchange = (DataExchangeInterface) UKDataContext.getContext().getBean("workorders") ;
				if(dataExchange!=null){
					map.addAttribute("workOrdersList", dataExchange.getListDataByIdAndOrgi(agentUserContacts.getContactsid(), super.getUser(request).getId(),  super.getOrgi(request))) ;
				}
				map.addAttribute("contactsid", agentUserContacts.getContactsid()) ;
			}
		}
		if(agentService!=null) {
			map.addAttribute("agentService", agentService) ;
		}
	}
	
}