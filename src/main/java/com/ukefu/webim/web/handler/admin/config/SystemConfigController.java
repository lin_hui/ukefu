package com.ukefu.webim.web.handler.admin.config;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.corundumstudio.socketio.SocketIOServer;
import com.hazelcast.core.HazelcastInstance;
import com.ukefu.core.ClusterContext;
import com.ukefu.core.UKDataContext;
import com.ukefu.util.Menu;
import com.ukefu.util.UKTools;
import com.ukefu.webim.service.cache.CacheHelper;
import com.ukefu.webim.service.repository.SecretRepository;
import com.ukefu.webim.service.repository.SystemConfigRepository;
import com.ukefu.webim.service.repository.SystemMessageRepository;
import com.ukefu.webim.service.repository.TemplateRepository;
import com.ukefu.webim.service.repository.UserRepository;
import com.ukefu.webim.web.handler.Handler;
import com.ukefu.webim.web.model.ReqlogWarningAction;
import com.ukefu.webim.web.model.Secret;
import com.ukefu.webim.web.model.SysDic;
import com.ukefu.webim.web.model.SystemConfig;
import com.ukefu.webim.web.model.SystemMessage;
import com.ukefu.webim.web.model.UKeFuDic;
import com.ukefu.webim.web.model.User;

@Controller
@RequestMapping("/admin/config")
public class SystemConfigController extends Handler{
	
	@Value("${uk.im.server.port}")  
    private Integer port;

	@Value("${web.upload-path}")
    private String path;
	
	@Autowired
	private SocketIOServer server ;
	
	@Autowired
	public HazelcastInstance hazelcastInstance;	
	
	@Autowired
	private SystemConfigRepository systemConfigRes ;
	
	
	@Autowired
	private SystemMessageRepository systemMessageRes ;
	
	@Autowired
	private SecretRepository secRes ;
	
	@Autowired
	private TemplateRepository templateRes ;
	
	@Autowired
	private UserRepository userRes ;
	
    @RequestMapping("/index")
    @Menu(type = "admin" , subtype = "config" , admin = true , spadmin = true)
    public ModelAndView index(ModelMap map , HttpServletRequest request , @Valid String execute) throws SQLException {
    	map.addAttribute("server", server) ;
    	
    	map.addAttribute("cluster", hazelcastInstance.getCluster().getMembers().toString()) ;
    	
    	map.addAttribute("master", ClusterContext.getInstance().isMaster()) ;
    	
    	if(UKDataContext.model.get("im")!=null){
    		map.addAttribute("entim", UKDataContext.model.get("im")) ;
    	}
    	if(request.getSession().getAttribute(UKDataContext.UKEFU_SYSTEM_INFOACQ)!=null){
    		map.addAttribute("entim", request.getSession().getAttribute(UKDataContext.UKEFU_SYSTEM_INFOACQ)) ;
    	}
    	map.addAttribute("server", server) ;
    	map.addAttribute("imServerStatus", UKDataContext.getIMServerStatus()) ;
    	List<Secret> secretConfig = secRes.findByOrgi(super.getOrgi(request)) ;
    	if(secretConfig!=null && secretConfig.size() > 0){
    		map.addAttribute("secret", secretConfig.get(0)) ;
    	}
    	List<SysDic> dicList = UKeFuDic.getInstance().getDic(UKDataContext.UKEFU_SYSTEM_DIC) ;
    	SysDic callCenterDic = null , workOrderDic = null  , smsDic = null , reqlogsmsDic = null , reqlogemailDic = null ;
    	for(SysDic dic : dicList){
    		if(dic.getCode().equals(UKDataContext.UKEFU_SYSTEM_CALLCENTER)){
    			callCenterDic = dic ;
    		}
    		if(dic.getCode().equals(UKDataContext.UKEFU_SYSTEM_WORKORDEREMAIL)){
    			workOrderDic = dic ;
    		}
    		if(dic.getCode().equals(UKDataContext.UKEFU_SYSTEM_SMSEMAIL)){
    			smsDic = dic ;
    		}
    		if(dic.getCode().equals(UKDataContext.UKEFU_SYSTEM_REQLOGSMS)){
    			reqlogsmsDic = dic ;
    		}
    		if(dic.getCode().equals(UKDataContext.UKEFU_SYSTEM_REQLOGEMAIL)){
    			reqlogemailDic = dic ;
    		}
    	}
    	if(callCenterDic!=null){
    		map.addAttribute("templateList", templateRes.findByTemplettypeAndOrgi(callCenterDic.getId(), super.getOrgi(request))) ;
    	}
    	if(workOrderDic!=null){
    		map.addAttribute("workOrderList", templateRes.findByTemplettypeAndOrgi(workOrderDic.getId(), super.getOrgi(request))) ;
    	}
    	if(smsDic!=null){
    		map.addAttribute("smsList", templateRes.findByTemplettypeAndOrgi(smsDic.getId(), super.getOrgi(request))) ;
    	}
    	if(reqlogsmsDic!=null){
    		map.addAttribute("reqlogsmstpList", templateRes.findByTemplettypeAndOrgi(reqlogsmsDic.getId(), super.getOrgi(request))) ;
    	}
    	if(reqlogemailDic!=null){
    		map.addAttribute("reqlogemailtpList", templateRes.findByTemplettypeAndOrgi(reqlogemailDic.getId(), super.getOrgi(request))) ;
    	}
    	
    	map.addAttribute("sysMessageList", systemMessageRes.findByMsgtypeAndOrgi(UKDataContext.SystemMessageType.EMAIL.toString(), super.getOrgi(request))) ;
    	
    	map.addAttribute("smsMessageList", systemMessageRes.findByMsgtypeAndOrgi(UKDataContext.SystemMessageType.SMS.toString(), super.getOrgi(request))) ;
    	
    	if(!StringUtils.isBlank(execute) && execute.equals("false")){
    		map.addAttribute("execute", execute) ;
    	}
    	if(!StringUtils.isBlank(request.getParameter("msg"))){
    		map.addAttribute("msg", request.getParameter("msg")) ;
    	}
    	map.addAttribute("userList", userRes.findAll());
		map.addAttribute("warnDicList" ,UKeFuDic.getInstance().getSysDic(UKDataContext.UKEFU_SYSTEM_REQLOG_WARNLV));
    	map.addAttribute("actionList" ,UKeFuDic.getInstance().getDic(UKDataContext.UKEFU_SYSTEM_REQLOG_ACTION));
        return request(super.createAdminTempletResponse("/admin/config/index"));
    }
    
    @RequestMapping("/save")
    @Menu(type = "admin" , subtype = "save" , admin = true , spadmin = true)
    public ModelAndView save(ModelMap map , HttpServletRequest request , @Valid SystemConfig config ,BindingResult result , @RequestParam(value = "keyfile", required = false) MultipartFile keyfile , @RequestParam(value = "loginlogo", required = false) MultipartFile loginlogo , @RequestParam(value = "consolelogo", required = false) MultipartFile consolelogo , @RequestParam(value = "favlogo", required = false) MultipartFile favlogo , @Valid Secret secret) throws SQLException, IOException, NoSuchAlgorithmException {
    	/*SystemConfig systemConfig = systemConfigRes.findByOrgi(super.getOrgi(request)) ;
    	config.setOrgi(super.getOrgi(request));*/
    	SystemConfig systemConfig = null ;
    	List<SystemConfig> configList = systemConfigRes.findByOrgi(UKDataContext.SYSTEM_ORGI) ;
    	if(configList != null && configList.size() > 0){
    		systemConfig = configList.get(0) ;
    	}
    	config.setOrgi(UKDataContext.SYSTEM_ORGI);
    	String msg = "0" ;
    	String jkspassword = null ;
    	if(StringUtils.isBlank(config.getJkspassword())){
    		config.setJkspassword(null);
    	}
    	if(systemConfig == null){
    		config.setCreater(super.getUser(request).getId());
    		config.setCreatetime(new Date());
    		systemConfig = config ;
    		jkspassword = UKTools.encryption(systemConfig.getJkspassword() );
    	}else{
    		UKTools.copyProperties(config,systemConfig);
    		if(StringUtils.isBlank(config.getJkspassword())){
    			jkspassword = systemConfig.getJkspassword();
        	}else {
        		jkspassword = UKTools.encryption(systemConfig.getJkspassword() );
        	}
    	}
    	if(config.isEnablessl()){
	    	if(keyfile!=null && keyfile.getBytes()!=null && keyfile.getBytes().length > 0 && keyfile.getOriginalFilename()!=null && keyfile.getOriginalFilename().length() > 0){
		    	FileUtils.writeByteArrayToFile(new File(path , "ssl/"+keyfile.getOriginalFilename()), keyfile.getBytes());
		    	systemConfig.setJksfile(keyfile.getOriginalFilename());
	    	}
	    	if(!StringUtils.isBlank(systemConfig.getJkspassword())) {
	    		File sslFilePath = new File(path , "ssl/https.properties") ;
		    	if(!sslFilePath.getParentFile().exists()) {
		    		sslFilePath.getParentFile().mkdirs() ;
		    	}
	    		Properties prop = new Properties();     
		    	FileOutputStream oFile = new FileOutputStream(sslFilePath);//true表示追加打开
		    	if(systemConfig.getJkspassword()!=null) {
		    		prop.setProperty("key-store-password", jkspassword) ;
		    	}
		    	prop.setProperty("key-store",systemConfig.getJksfile()) ;
		    	prop.store(oFile , "SSL Properties File");
		    	oFile.close();
	    	}
    	}else if(new File(path , "ssl").exists()){
    		File[] sslFiles = new File(path , "ssl").listFiles() ;
    		for(File sslFile : sslFiles){
    			sslFile.delete();
    		}
    	}
    	
    	if(loginlogo!=null && !StringUtils.isBlank(loginlogo.getOriginalFilename()) && loginlogo.getOriginalFilename().lastIndexOf(".") > 0) {
    		String logoFileName = "logo/"+UKTools.md5(loginlogo.getOriginalFilename())+loginlogo.getOriginalFilename().substring(loginlogo.getOriginalFilename().lastIndexOf(".")) ;
    		FileUtils.writeByteArrayToFile(new File(path ,logoFileName), loginlogo.getBytes());
    		systemConfig.setLoginlogo(logoFileName);
    	}
    	if(consolelogo!=null && !StringUtils.isBlank(consolelogo.getOriginalFilename()) && consolelogo.getOriginalFilename().lastIndexOf(".") > 0) {
    		String consoleLogoFileName = "logo/"+UKTools.md5(consolelogo.getOriginalFilename())+consolelogo.getOriginalFilename().substring(consolelogo.getOriginalFilename().lastIndexOf(".")) ;
    		FileUtils.writeByteArrayToFile(new File(path ,consoleLogoFileName), consolelogo.getBytes());
    		systemConfig.setConsolelogo(consoleLogoFileName);
    	}
    	if(favlogo!=null && !StringUtils.isBlank(favlogo.getOriginalFilename()) && favlogo.getOriginalFilename().lastIndexOf(".") > 0) {
    		String favLogoFileName = "logo/"+UKTools.md5(favlogo.getOriginalFilename())+favlogo.getOriginalFilename().substring(favlogo.getOriginalFilename().lastIndexOf(".")) ;
    		FileUtils.writeByteArrayToFile(new File(path ,favLogoFileName), favlogo.getBytes());
    		systemConfig.setFavlogo(favLogoFileName);
    	}
    	
    	if(secret!=null && !StringUtils.isBlank(secret.getPassword())){
	    	List<Secret> secretConfig = secRes.findByOrgi(super.getOrgi(request)) ;
	    	String repassword = request.getParameter("repassword") ;
	    	if(!StringUtils.isBlank(repassword) && repassword.equals(secret.getPassword())){
		    	if(secretConfig!=null && secretConfig.size() > 0){
		    		Secret tempSecret = secretConfig.get(0) ;
		    		String oldpass = request.getParameter("oldpass") ;
		    		if(!StringUtils.isBlank(oldpass) && UKTools.md5(oldpass).equals(tempSecret.getPassword())){
		    			tempSecret.setPassword(UKTools.md5(secret.getPassword()));
		    			msg = "1" ;
		    			tempSecret.setEnable(true);
		    			secRes.save(tempSecret) ;
		    		}else{
			    		msg = "3" ;
			    	}
		    	}else{
		    		secret.setOrgi(super.getOrgi(request));
		    		secret.setCreater(super.getUser(request).getId());
		    		secret.setCreatetime(new Date());
		    		secret.setPassword(UKTools.md5(secret.getPassword()));
		    		secret.setEnable(true);
		    		msg = "1" ;
		    		secRes.save(secret) ;
		    	}
	    	}else{
	    		msg = "2" ;
	    	}
	    	map.addAttribute("msg", msg) ;
    	}
    	
    	if(!StringUtils.isBlank(jkspassword)) {
    		systemConfig.setJkspassword(jkspassword);
    	}
    	systemConfig.setReqlogwarningtouser(config.getReqlogwarningtouser());
    	
    	//把预警用户放入缓存
    	if (!StringUtils.isBlank(config.getReqlogwarningtouser())) {
    		List<User> userList = new ArrayList<User>();
    		String[] userids = systemConfig.getReqlogwarningtouser().split(",");
    		if (userids != null && userids.length >0) {
    			for(String id : userids) {
    				User ut = userRes.findById(id) ;
    				userList.add(ut);
    			}
			}
    		CacheHelper.getSystemCacheBean().put(UKDataContext.SYSTEM_CACHE_WARNING_TOUSER, userList , super.getOrgi(request));
		}
    	if (!StringUtils.isBlank(config.getReqlogwarningemailid())) {//把预警邮件服务存入缓存
			SystemMessage systemMessage = systemMessageRes.findByIdAndOrgi(config.getReqlogwarningemailid(),super.getOrgi(request)) ;
    		CacheHelper.getSystemCacheBean().put(config.getReqlogwarningemailid(), systemMessage , super.getOrgi(request));
		}
		if (!StringUtils.isBlank(config.getReqlogwarningsmsid())) {//把预警短信服务存入缓存
			SystemMessage systemMessage = systemMessageRes.findByIdAndOrgi(config.getReqlogwarningsmsid(),super.getOrgi(request)) ;
    		CacheHelper.getSystemCacheBean().put(config.getReqlogwarningsmsid(), systemMessage , super.getOrgi(request));
		}
    	List<ReqlogWarningAction> reqlogWarningActionList= new ArrayList<ReqlogWarningAction>();
    	if (request.getParameterValues("reqlogwarningaction") != null) {
			String[] actions = request.getParameterValues("reqlogwarningaction");
			if (actions != null && actions.length > 0) {
				for(String action : actions) {
					String[] warnparams = action.split(":");
					if (warnparams != null && warnparams.length>0) {
						ReqlogWarningAction reqlogWarningAction = new ReqlogWarningAction();
						reqlogWarningAction.setWarnid(warnparams[0]);
						reqlogWarningAction.setActionid(warnparams.length>1?warnparams[1]:null);
						reqlogWarningActionList.add(reqlogWarningAction) ;
					}
				}
				
			}
		}
    	systemConfig.setReqlogwarningaction(reqlogWarningActionList.size() > 0 ?UKTools.toJson(reqlogWarningActionList).replaceAll("\r", "").replaceAll("\n", "").replaceAll("\t", ""):"");
    	systemConfigRes.save(systemConfig) ;
    	
    	CacheHelper.getSystemCacheBean().put("systemConfig", systemConfig , super.getOrgi(request));
    	map.addAttribute("imServerStatus", UKDataContext.getIMServerStatus()) ;
    	
    	return request(super.createRequestPageTempletResponse("redirect:/admin/config/index.html?msg="+msg));
    }
    
    @RequestMapping("/warnadd")
    @Menu(type = "admin" , subtype = "warnadd" , admin = true , spadmin = true)
    public ModelAndView warnadd(ModelMap map , HttpServletRequest request) {
    	
    	
    	
    	return request(super.createRequestPageTempletResponse("/admin/config/warnadd"));
    }
}