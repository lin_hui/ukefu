package com.ukefu.webim.web.model;

import com.ukefu.util.event.UserEvent;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Proxy;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "uk_callcenter_event")
@Proxy(lazy = false)
public class StatusEventHangup implements Serializable,UserEvent{

	private static final long serialVersionUID = -7147175985175608937L;

	private String id ;
	private Date updatetime = new Date() ;

	private String creater ;		//变更用处，标识是否已接通
	private String servicestatus ;	//通话状态
	private int ringduration ;//振铃时长
	
	
	private String hangupsip ;	//SIP消息记录
	
	private String quene ;		//呼入队列
	
	private Date endtime ;//通话结束时间
	
	private int duration ;//通话时长

	private String agent ;//坐席工号
	private String name;//
	
	private boolean callstatus ;	//拨打状态  ， 成功或失败
	
	private String contactsid ;//
	
	private String bridgeid ;			//桥接对方ID
	private boolean bridge ;			//是否桥接
	
	private boolean misscall = true;	//是否漏话
	
	private boolean satisf ;	//是否记录满意度调查
	private String satisfaction 	;		//满意度评价
	private Date satisfdate ;				//满意度调查提交时间
	
	private String hangupcase ;		//挂断原因	
	private String hangupinitiator ;//挂断发起方

	
	private String itemid ;			//项目ID

	private String callresult;

	private String username;
	private String organ;

	@Id
	@Column(length = 32)
	@GeneratedValue(generator = "system-uuid")
	@GenericGenerator(name = "system-uuid", strategy = "assigned")
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}

	public Date getUpdatetime() {
		return updatetime;
	}

	public void setUpdatetime(Date updatetime) {
		this.updatetime = updatetime;
	}

	public String getCreater() {
		return creater;
	}

	public void setCreater(String creater) {
		this.creater = creater;
	}

	public String getServicestatus() {
		return servicestatus;
	}

	public void setServicestatus(String servicestatus) {
		this.servicestatus = servicestatus;
	}

	public int getRingduration() {
		return ringduration;
	}

	public void setRingduration(int ringduration) {
		this.ringduration = ringduration;
	}

	public String getHangupsip() {
		return hangupsip;
	}

	public void setHangupsip(String hangupsip) {
		this.hangupsip = hangupsip;
	}

	public String getQuene() {
		return quene;
	}

	public void setQuene(String quene) {
		this.quene = quene;
	}

	public Date getEndtime() {
		return endtime;
	}

	public void setEndtime(Date endtime) {
		this.endtime = endtime;
	}

	public int getDuration() {
		return duration;
	}

	public void setDuration(int duration) {
		this.duration = duration;
	}

	public String getAgent() {
		return agent;
	}

	public void setAgent(String agent) {
		this.agent = agent;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean isCallstatus() {
		return callstatus;
	}

	public void setCallstatus(boolean callstatus) {
		this.callstatus = callstatus;
	}

	public String getContactsid() {
		return contactsid;
	}

	public void setContactsid(String contactsid) {
		this.contactsid = contactsid;
	}

	public String getBridgeid() {
		return bridgeid;
	}

	public void setBridgeid(String bridgeid) {
		this.bridgeid = bridgeid;
	}

	public boolean isBridge() {
		return bridge;
	}

	public void setBridge(boolean bridge) {
		this.bridge = bridge;
	}

	public boolean isMisscall() {
		return misscall;
	}

	public void setMisscall(boolean misscall) {
		this.misscall = misscall;
	}

	public boolean isSatisf() {
		return satisf;
	}

	public void setSatisf(boolean satisf) {
		this.satisf = satisf;
	}

	public String getSatisfaction() {
		return satisfaction;
	}

	public void setSatisfaction(String satisfaction) {
		this.satisfaction = satisfaction;
	}

	public Date getSatisfdate() {
		return satisfdate;
	}

	public void setSatisfdate(Date satisfdate) {
		this.satisfdate = satisfdate;
	}

	public String getHangupcase() {
		return hangupcase;
	}

	public void setHangupcase(String hangupcase) {
		this.hangupcase = hangupcase;
	}

	public String getHangupinitiator() {
		return hangupinitiator;
	}

	public void setHangupinitiator(String hangupinitiator) {
		this.hangupinitiator = hangupinitiator;
	}

	public String getItemid() {
		return itemid;
	}

	public void setItemid(String itemid) {
		this.itemid = itemid;
	}

	public String getCallresult() {
		return callresult;
	}

	public void setCallresult(String callresult) {
		this.callresult = callresult;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getOrgan() {
		return organ;
	}

	public void setOrgan(String organ) {
		this.organ = organ;
	}
}
