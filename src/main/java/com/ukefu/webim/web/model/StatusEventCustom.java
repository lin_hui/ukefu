package com.ukefu.webim.web.model;

import com.ukefu.util.event.UserEvent;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Proxy;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "uk_callcenter_event")
@Proxy(lazy = false)
public class StatusEventCustom implements Serializable,UserEvent{

	private static final long serialVersionUID = 6380455081348698661L;

	private String id ;
	private Date updatetime = new Date() ;
	
	
	private String servicestatus ;	//通话状态
	
	private Date inqueuetime ;		//进队列时间
	private Date outqueuetime ;		//进队列时间
	private int queuetime ;			//排队时长
	

	private String membersessionid ;	//转接前通话ID
	
	private String agent ;//坐席工号

	@Id
	@Column(length = 32)
	@GeneratedValue(generator = "system-uuid")
	@GenericGenerator(name = "system-uuid", strategy = "assigned")
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}


	public Date getUpdatetime() {
		return updatetime;
	}

	public void setUpdatetime(Date updatetime) {
		this.updatetime = updatetime;
	}

	public String getServicestatus() {
		return servicestatus;
	}

	public void setServicestatus(String servicestatus) {
		this.servicestatus = servicestatus;
	}

	public Date getInqueuetime() {
		return inqueuetime;
	}

	public void setInqueuetime(Date inqueuetime) {
		this.inqueuetime = inqueuetime;
	}

	public Date getOutqueuetime() {
		return outqueuetime;
	}

	public void setOutqueuetime(Date outqueuetime) {
		this.outqueuetime = outqueuetime;
	}

	public int getQueuetime() {
		return queuetime;
	}

	public void setQueuetime(int queuetime) {
		this.queuetime = queuetime;
	}

	public String getMembersessionid() {
		return membersessionid;
	}

	public void setMembersessionid(String membersessionid) {
		this.membersessionid = membersessionid;
	}

	public String getAgent() {
		return agent;
	}

	public void setAgent(String agent) {
		this.agent = agent;
	}
}
