package com.ukefu.webim.web.model;

import com.ukefu.util.event.UserEvent;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Proxy;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "uk_callcenter_event")
@Proxy(lazy = false)
public class StatusEventDtmf implements Serializable,UserEvent{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3220117068446530956L;

	private String id ;

	private boolean dtmf ;		//是否记录了DTMF信息
	private String dtmfrec ;	//DTMF记录
	

	@Id
	@Column(length = 32)
	@GeneratedValue(generator = "system-uuid")
	@GenericGenerator(name = "system-uuid", strategy = "assigned")
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}

	public boolean isDtmf() {
		return dtmf;
	}

	public void setDtmf(boolean dtmf) {
		this.dtmf = dtmf;
	}

	public String getDtmfrec() {
		return dtmfrec;
	}

	public void setDtmfrec(String dtmfrec) {
		this.dtmfrec = dtmfrec;
	}
}
