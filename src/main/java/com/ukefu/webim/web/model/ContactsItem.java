package com.ukefu.webim.web.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.annotations.GenericGenerator;

import com.ukefu.util.UKTools;
import com.ukefu.webim.util.OnlineUserUtils;

/**
 * 联系人项目分类
 *
 */
@Entity
@Table(name = "uk_contacts_item")
@org.hibernate.annotations.Proxy(lazy = false)
public class ContactsItem implements java.io.Serializable {

	private static final long serialVersionUID = 7484598945508868852L;
	
	private String id = UKTools.getUUID();
	private String name ;
	private String creater ;
	private String updater ;
	private Date createtime = new Date();
	private Date updatetime ;
	private String itemid;
	private String parentid;
	private String type;//联系人或客户
	private String orgi ;
	
	private String statuscode;
	
	private boolean autodis;	//自动分配
	private String express ;		//自动流转的 配置信息
	private String targetdis ;		//自动流转的 id分配次数 ContactsItemTargetdis
	
	private boolean autorecycle;	//自动回收
	private int recycletime ;	//自动回收周期 （小时）
		
	
	
	@Id
	@Column(length = 32)
	@GeneratedValue(generator = "system-uuid")
	@GenericGenerator(name = "system-uuid", strategy = "assigned")
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCreater() {
		return creater;
	}
	public void setCreater(String creater) {
		this.creater = creater;
	}
	public Date getCreatetime() {
		return createtime;
	}
	public void setCreatetime(Date createtime) {
		this.createtime = createtime;
	}
	public Date getUpdatetime() {
		return updatetime;
	}
	public void setUpdatetime(Date updatetime) {
		this.updatetime = updatetime;
	}
	public String getOrgi() {
		return orgi;
	}
	public void setOrgi(String orgi) {
		this.orgi = orgi;
	}
	public String getUpdater() {
		return updater;
	}
	public void setUpdater(String updater) {
		this.updater = updater;
	}
	public String getItemid() {
		return itemid;
	}
	public void setItemid(String itemid) {
		this.itemid = itemid;
	}
	public String getParentid() {
		return parentid;
	}
	public void setParentid(String parentid) {
		this.parentid = parentid;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getStatuscode() {
		return statuscode;
	}
	public void setStatuscode(String statuscode) {
		this.statuscode = statuscode;
	}
	public boolean isAutodis() {
		return autodis;
	}
	public void setAutodis(boolean autodis) {
		this.autodis = autodis;
	}
	public boolean isAutorecycle() {
		return autorecycle;
	}
	public void setAutorecycle(boolean autorecycle) {
		this.autorecycle = autorecycle;
	}
	public int getRecycletime() {
		return recycletime;
	}
	public void setRecycletime(int recycletime) {
		this.recycletime = recycletime;
	}
	public String getExpress() {
		return express;
	}
	public void setExpress(String express) {
		this.express = express;
	}
	public String getTargetdis() {
		return targetdis;
	}
	public void setTargetdis(String targetdis) {
		this.targetdis = targetdis;
	}
	
	@Transient
	public List<ContactsItemExpress> getItemExpress(){
		List<ContactsItemExpress> contactsItemExpress = null ;
		if(!StringUtils.isBlank(this.getExpress())) {
			try {
				contactsItemExpress = OnlineUserUtils.objectMapper.readValue(this.getExpress(), UKTools.getCollectionType(ArrayList.class, ContactsItemExpress.class))  ;
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return contactsItemExpress ;
	}
	@Transient
	public List<ContactsItemTargetdis> getItemTargetdis(){
		List<ContactsItemTargetdis> contactsItemTargetdis = null ;
		if(!StringUtils.isBlank(this.getTargetdis())) {
			try {
				contactsItemTargetdis = OnlineUserUtils.objectMapper.readValue(this.getTargetdis(), UKTools.getCollectionType(ArrayList.class, ContactsItemTargetdis.class))  ;
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return contactsItemTargetdis ;
	}
	
}
