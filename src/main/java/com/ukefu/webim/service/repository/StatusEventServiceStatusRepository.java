package com.ukefu.webim.service.repository;

import com.ukefu.webim.web.model.StatusEventServicestatus;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StatusEventServiceStatusRepository extends JpaRepository<StatusEventServicestatus, String> {

}
