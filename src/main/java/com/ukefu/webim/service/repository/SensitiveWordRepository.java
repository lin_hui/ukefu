package com.ukefu.webim.service.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;

import com.ukefu.webim.web.model.SensitiveWord;

public abstract interface SensitiveWordRepository extends JpaRepository<SensitiveWord, String>{
	
	public abstract SensitiveWord findByIdAndOrgi(String id, String orgi);
	
	public abstract Page<SensitiveWord> findAll(Specification<SensitiveWord> spec, Pageable page) ;
	
	public abstract int countByKeywordAndTypeAndOrgi(String word,String type, String orgi);
}
