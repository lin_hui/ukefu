package com.ukefu.webim.service.repository;

import com.ukefu.webim.web.model.StatusEventMisscall;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StatusEventMisscallRepository extends JpaRepository<StatusEventMisscall, String> {

}
