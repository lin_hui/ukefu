package com.ukefu.webim.service.repository;

import com.ukefu.webim.web.model.StatusEventHold;
import com.ukefu.webim.web.model.StatusEventRecord;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StatusEventHoldRepository extends JpaRepository<StatusEventHold, String> {

}
