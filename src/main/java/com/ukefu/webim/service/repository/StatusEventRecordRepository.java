package com.ukefu.webim.service.repository;

import com.ukefu.webim.web.model.StatusEventAnswer;
import com.ukefu.webim.web.model.StatusEventRecord;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StatusEventRecordRepository extends JpaRepository<StatusEventRecord, String> {

}
