package com.ukefu.webim.service.es;

import java.util.List;

import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

import com.ukefu.webim.web.model.MRoundQuestion;

public interface MRoundQuestionRepository
extends  ElasticsearchRepository<MRoundQuestion, String>
{
	public abstract List<MRoundQuestion> findByDataidAndOrgi(String dataid , String orgi);
	
	public abstract List<MRoundQuestion> findByParentidAndOrgi(String parentid , String orgi);
	
	public abstract MRoundQuestion findById(String id);
}
